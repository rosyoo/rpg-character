package Item;

import Item.Item;

public class Weapons extends Item{
    public WeaponTypes weaponType;
    public double weaponDps;
    public double dmg;
    public double attackSpeed;

    public enum WeaponTypes{ //Types of weapon
        AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
    }

    public Weapons(String weaponName, int itemLevel, Slot slot, WeaponTypes weaponType, int dmg, double attackSpeed){
        //this.weaponDps = (dmg * attackSpeed);
        this.weaponType = weaponType;
        this.setItemName(weaponName);
        this.setItemLevel(itemLevel);
        this.dmg = dmg;
        this.attackSpeed = attackSpeed;
        this.setSlot(slot.WEAPON);
    }

    public Weapons(){}

    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    public double getWeaponDps() {
        return this.weaponDps = (this.dmg * this.attackSpeed);
    }
    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double getDmg() {
        return dmg;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public void setWeaponDps(int weaponDps) {
        this.weaponDps = weaponDps;
    }

    public void setWeaponType(WeaponTypes weaponType) {
        this.weaponType = weaponType;
    }
}
