package Item;

import Item.Item;
import attributes.PrimaryAttributes;

public class Armours extends Item{
    public PrimaryAttributes attributes;
    public ArmourTypes armourType;

    public enum ArmourTypes{ //Types of armour
        CLOTH, LEATHER, MAIL, PLATE
    }

    public Armours(String armorName, int itemLevel, Slot slot, ArmourTypes armourType, int strength, int dexterity, int intelligence){
        this.setItemName(armorName);
        this.setItemLevel(itemLevel);
        this.armourType = armourType;
        this.setSlot(slot);
        this.attributes = new PrimaryAttributes(strength, dexterity, intelligence);
    }
    //public Armours(){}

    public ArmourTypes getArmourType(){
        return armourType;
    }

    public void setArmourType(ArmourTypes armourType){
        this.armourType = armourType;
    }

}
