package Item;

public abstract class Item {
    public Slot slot;
    public String itemName;
    public int itemLevel;

    public enum Slot{
        HEAD, BODY, LEGS, WEAPON
    }
    public Slot getSlot(){
        return slot;
    }
    public void setSlot(Slot slot){
        this.slot = slot;
    }
    public String getItemName(){
        return itemName;
    }
    public void setItemName(String itemName){
        this.itemName = itemName;
    }
    public int getItemLevel(){
        return itemLevel;
    }
    public void setItemLevel(int itemLevel){
        this.itemLevel = itemLevel;
    }
}
