package characters;

import Item.Item;
import Item.Weapons;
import Item.Armours;
import attributes.BaseAttributes;
import attributes.PrimaryAttributes;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

import java.util.HashMap;

public abstract class Character {
    public String name;
    public int level = 1; //Hero starts at level 1
    public double dmgAttribute;
    public String classType;
    public BaseAttributes baseAttribute; //Base primary attributes
    public PrimaryAttributes totalPrimaryAttribute; // used as base + armor attributes
    public HashMap<Item.Slot, Item> equipments = new HashMap<>();
    public abstract boolean equipWeapon(Weapons weapon) throws InvalidWeaponException;
    public abstract boolean equipArmour(Armours armour) throws InvalidArmorException;

    public Character(String name, String classType, BaseAttributes baseAttribute){
        this.name = name;
        this.classType = classType;
        this.baseAttribute = baseAttribute;
    }
    public BaseAttributes getBaseAttribute(){
        return baseAttribute;
    }

    public abstract void levelUpGainAttribute();

    public double charDPS;

    public PrimaryAttributes getTotalPrimaryAttribute(){
        return totalPrimaryAttribute;
    }

    public HashMap<Item.Slot, Item> getEquipments(){
        return equipments;
    }

    public int getLevel(){
        return level;
    }

    public double getCharDPS(){
        this.dmgAttribute = this.getBaseAttribute().strength;
        Weapons weapon = (Weapons) this.equipments.get(Item.Slot.WEAPON);
        if(weapon != null){ //If weapon is equipped
            this.charDPS = weapon.getWeaponDps() * (1 + (this.dmgAttribute / 100));
        }else{
            this.charDPS = ( 1 + (this.dmgAttribute / 100));
        }
        return charDPS;
    }
    public void setLevel(int level) {
        this.level = level;
    }

    public void setTotalPrimaryAttribute(PrimaryAttributes totalPrimaryAttribute) {
        this.totalPrimaryAttribute = totalPrimaryAttribute;
    }

    public void setBaseAttribute(BaseAttributes baseAttribute) {
        this.baseAttribute = baseAttribute;
    }

    @Override //Object as string
    public String toString(){
        return "Character name: " + name + "\n" + "Character level: "+ level + "\n"+
                "Strength: "+ baseAttribute.strength + "\n"+
                "Dexterity: "+ baseAttribute.dexterity+ "\n"+
                "Intelligence: "+baseAttribute.intelligence + "\n"+
                "DPS: "+ this.getCharDPS();
    }
}
