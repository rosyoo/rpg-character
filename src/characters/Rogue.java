package characters;

import attributes.BaseAttributes;
import attributes.PrimaryAttributes;
import exceptions.InvalidWeaponException;
import exceptions.InvalidArmorException;
import Item.Weapons;
import Item.Armours;

public class Rogue extends Character{
    public Rogue(String name){ //Rogue object with initial values
        super(name, "Rogue", new BaseAttributes(2, 6, 1));
    }

    @Override
    public void levelUpGainAttribute(){ //Rogue levels up -> gains attributes
        this.setLevel(this.getLevel() + 1);
        BaseAttributes attributes = this.getBaseAttribute();
        attributes.strength = attributes.strength + 1;
        attributes.dexterity = attributes.dexterity + 4;
        attributes.intelligence = attributes.intelligence + 1;
        this.setBaseAttribute(attributes);
    }

    @Override
    //Check Rogue equipment
    //Rogue can only equip DAGGER or SWORD
    //Rogue can't equip weapon of higher level
    public boolean equipWeapon(Weapons weapon) throws InvalidWeaponException{
        if(weapon.itemLevel > this.level){
            throw new InvalidWeaponException("Weapon is of higher level! Required weapon level is: "+weapon.itemLevel);
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.DAGGER){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.SWORD){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else{
            throw new InvalidWeaponException("A Rogue can only equip DAGGER or SWORD as weapon");
        }
    }

    @Override
    //Check Rogue equipment
    //Rogue can only equip LEATHER and MAIL
    //Rogue can't equip armour of higher level
    public boolean equipArmour(Armours armour) throws InvalidArmorException{
        if(armour.itemLevel > this.level){
            throw new InvalidArmorException("Armour is of higher level! Required armour level is: "+armour.itemLevel);
        }else if(armour.getArmourType() == Armours.ArmourTypes.LEATHER){
            this.equipments.put(armour.getSlot(), armour);
            return true;
        }else if(armour.getArmourType() == Armours.ArmourTypes.MAIL){
            this.equipments.put(armour.getSlot(), armour);
            return true;
        }else{
            throw new InvalidArmorException("A Rogue can only wear LEATHER or MAIL as armour");
        }

    }

}
