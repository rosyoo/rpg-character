package characters;

import attributes.BaseAttributes;
import attributes.PrimaryAttributes;
import exceptions.InvalidWeaponException;
import exceptions.InvalidArmorException;
import Item.Weapons;
import Item.Armours;

public class Mage extends Character{
    public Mage(String name){ //Mage object with initial values
        super(name, "Mage", new BaseAttributes(1, 1, 8));
    }

    @Override
    public void levelUpGainAttribute(){ //Mage levels up -> gains attributes
        this.setLevel(this.getLevel() + 1);
        BaseAttributes attributes = this.getBaseAttribute();
        attributes.strength = attributes.strength + 1;
        attributes.dexterity = attributes.dexterity + 1;
        attributes.intelligence = attributes.intelligence + 5;
        this.setBaseAttribute(attributes);
    }

    @Override
    //Check Mage equipment
    //Mage can only equip STAFF, WAND
    //Mage can't equip weapon of higher level
    public boolean equipWeapon(Weapons weapon) throws InvalidWeaponException{
        if(weapon.itemLevel > this.level){
            throw new InvalidWeaponException("Weapon is of higher level! Required weapon level is: "+weapon.itemLevel);
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.STAFF){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.WAND){
            this.equipments.put(weapon.getSlot(), weapon);
            return true;
        }else{
            throw new InvalidWeaponException("A Mage can only equip STAFF or WAND as weapon");
        }
    }

    @Override
    //Check Mage equipment
    //Mage can only equip CLOTH
    //Mage can't equip armour of higher level
    public boolean equipArmour(Armours armour) throws InvalidArmorException{
        if(armour.itemLevel > this.level){
            throw new InvalidArmorException("Armour is of higher level! Required armour level is: "+armour.itemLevel);
        }else if(armour.getArmourType() == Armours.ArmourTypes.CLOTH){
            this.equipments.put(armour.getSlot(), armour);
            return true;
        }else{
            throw new InvalidArmorException("A Mage can only wear CLOTH as armour");
        }

    }

}
