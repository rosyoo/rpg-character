package com.company;

import characters.*;
import characters.Character;
import exceptions.InvalidWeaponException;
import exceptions.InvalidArmorException;
import Item.Armours;
import Item.Item;
import Item.Weapons;

public class Main {

    public static void main(String[] args) throws InvalidArmorException, InvalidWeaponException{

        Warrior warrior = new Warrior("Haldor");
        Mage mage = new Mage("Morgana");
        Ranger ranger = new Ranger("Legolas");
        Rogue rogue = new Rogue("Ezio");

        System.out.println(warrior);
        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);

        warrior.levelUpGainAttribute();
        mage.levelUpGainAttribute();
        ranger.levelUpGainAttribute();
        rogue.levelUpGainAttribute();

        System.out.println(warrior);
        System.out.println(mage);
        System.out.println(ranger);
        System.out.println(rogue);

        Weapons testWeapon = new Weapons();
        testWeapon.setItemName("Common Axe");
        testWeapon.setItemLevel(1);
        testWeapon.setSlot(Item.Slot.WEAPON);
        testWeapon.setWeaponType(Weapons.WeaponTypes.AXE);
        testWeapon.setDmg(7);
        testWeapon.setAttackSpeed(1.1);

        warrior.equipWeapon(testWeapon);
        System.out.println(warrior);

        Armours testPlateBody = new Armours("Common Plate Body Armour",1, Item.Slot.BODY, Armours.ArmourTypes.PLATE,1,0,0);
        /*testPlateBody.setItemName("Common Plate Body Armour");
        testPlateBody.setItemLevel(1);
        testPlateBody.setSlot(Item.Slot.BODY);
        testPlateBody.setArmourType(Armours.ArmourTypes.PLATE);*/

        warrior.equipArmour(testPlateBody);
        System.out.println(warrior);



    }
}
