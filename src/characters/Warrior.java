package characters;

import attributes.BaseAttributes;
import attributes.PrimaryAttributes;
import exceptions.InvalidWeaponException;
import exceptions.InvalidArmorException;
import Item.Weapons;
import Item.Armours;

public class Warrior extends Character{

    public Warrior(String name){ //Warrior object with initial values
        super(name, "Warrior", new BaseAttributes(5, 2, 1));
    }

    @Override
    public void levelUpGainAttribute(){ //Warrior levels up -> gains attributes
        this.setLevel(this.getLevel() + 1);
        BaseAttributes attributes = this.getBaseAttribute();
        attributes.strength = attributes.strength + 3;
        attributes.dexterity = attributes.dexterity + 2;
        attributes.intelligence = attributes.intelligence + 1;
        this.setBaseAttribute(attributes);
    }

    @Override
    //Check warrior equipment
    //Warrior can only equip AXE, HAMMER, SWORD
    //Warrior can't equip weapon of higher level
    public boolean equipWeapon(Weapons weapon) throws InvalidWeaponException{
        if(weapon.itemLevel > this.level){
            throw new InvalidWeaponException("Weapon is of higher level! Required weapon level is: "+weapon.itemLevel);
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.AXE){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.HAMMER){
            this.equipments.put(weapon.getSlot(), weapon);
            return true;
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.SWORD){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else{
            throw new InvalidWeaponException("A warrior can only equip Axe, Hammer or Sword.");
        }
    }

    @Override
    //Check warrior equipment
    //Warrior can only equip MAIL, PLATE
    //Warrior can't equip armour of higher level
    public boolean equipArmour(Armours armour) throws InvalidArmorException{
        if(armour.itemLevel > this.level){
            throw new InvalidArmorException("Armour is of higher level! Required armour level is: "+armour.itemLevel);
        }else if(armour.getArmourType() == Armours.ArmourTypes.MAIL){
            this.equipments.put(armour.getSlot(), armour);
            this.setTotalPrimaryAttribute(totalPrimaryAttribute);
            return true;
        }else if(armour.getArmourType() == Armours.ArmourTypes.PLATE){
            this.equipments.put(armour.getSlot(), armour);
            this.setTotalPrimaryAttribute(totalPrimaryAttribute);
            return true;
        }else{
            throw new InvalidArmorException("A warrior can only wear Mail or Plate as armour");
        }

    }

}
