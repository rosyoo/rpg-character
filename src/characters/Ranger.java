package characters;

import attributes.BaseAttributes;
import attributes.PrimaryAttributes;
import exceptions.InvalidWeaponException;
import exceptions.InvalidArmorException;
import Item.Weapons;
import Item.Armours;

public class Ranger extends Character {
    public Ranger(String name){ //Ranger object with initial values
        super(name, "Ranger", new BaseAttributes(1, 7, 1));
    }

    @Override
    public void levelUpGainAttribute(){ //Ranger levels up -> gains attributes
        this.setLevel(this.getLevel() + 1);
        BaseAttributes attributes = this.getBaseAttribute();
        attributes.strength = attributes.strength + 1;
        attributes.dexterity = attributes.dexterity + 5;
        attributes.intelligence = attributes.intelligence + 1;
        this.setBaseAttribute(attributes);
    }

    @Override
    //Check Ranger equipment
    //Ranger can only equip BOW
    //Ranger can't equip weapon of higher level
    public boolean equipWeapon(Weapons weapon) throws InvalidWeaponException{
        if(weapon.itemLevel > this.level){
            throw new InvalidWeaponException("Weapon is of higher level! Required weapon level is: "+weapon.itemLevel);
        }else if(weapon.getWeaponType() == Weapons.WeaponTypes.BOW){
            this.equipments.put(weapon.getSlot(),weapon);
            return true;
        }else{
            throw new InvalidWeaponException("A Ranger can only equip BOW as weapon");
        }
    }

    @Override
    //Check Ranger equipment
    //Ranger can only equip LEATHER and MAIL
    //Ranger can't equip armour of higher level
    public boolean equipArmour(Armours armour) throws InvalidArmorException{
        if(armour.itemLevel > this.level){
            throw new InvalidArmorException("Armour is of higher level! Required armour level is: "+armour.itemLevel);
        }else if(armour.getArmourType() == Armours.ArmourTypes.LEATHER){
            this.equipments.put(armour.getSlot(), armour);
            return true;
        }else if(armour.getArmourType() == Armours.ArmourTypes.MAIL){
            this.equipments.put(armour.getSlot(), armour);
            return true;
        }else{
            throw new InvalidArmorException("A Ranger can only wear LEATHER or MAIL as armour");
        }

    }

}
